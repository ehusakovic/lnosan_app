import os
from flask import Flask, render_template, session, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


from models import Result


@app.route('/', methods=['GET'])
def index():
    if request.method == 'POST':
        if 'get_data' in request.form:
            try:
                users = Result.query.all()

            except Exception as error:
                users = {}
                pass

            return render_template(
                'index.html',
                users = users)

    elif request.method == 'GET':  
        try:
            users = Result.query.all()
            print(users)

        except Exception as error:
            print(error)
            users={}
            pass

        return render_template(
            'index.html',
            users = users)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
    
