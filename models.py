from app import db


class Result(db.Model):
    __tablename__ = 'users'
    __table_args__ = {'schema': 'sample'}


    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('Name', db.String())
    surname = db.Column('Surname', db.String())
    address = db.Column('Address', db.String())

    def __init__(self, name, surname, address):
        self.name = name
        self.surname = surname
        self.address = address

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'name': self.name,
            'surname': self.surname,
            'address': self.address
        }
