FROM python:3.7-slim-buster AS test_app

ENV FLASK_ENV=production

WORKDIR /app
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["gunicorn", "--bind", "0.0.0.0:5002", "app:app"]

EXPOSE 5002

